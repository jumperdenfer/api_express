const express = require('express')
const app = express()

// création d'un middleware affichant la Date

// callback contenant la date actuelle
var requestTime = function(req,res,next){
  var today = new Date();
  var minute = today.getMinutes();
  var hours = today.getHours();
  var day = today.getDate();
  var month = today.getMonth()+1;
  var year = today.getFullYear();
  var date_current = hours +':' + minute + ' le ' + day + '-' + month +'-'+ year

  req.requestTime = date_current;
  next();
};
// appel de requestTime
app.use(requestTime)

// Création de la route incluant le callback qui contient la date actuelle
app.get('/', function(req,res){
  var text = "Test de middleware<br>";
  text += '<p>envoyé à ' + req.requestTime + '</p>'
  res.send(text)

})


// Définit le port du serveur
app.listen(4100,() => console.log("Your app listen on port 4100"));
