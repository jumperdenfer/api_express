var sql = {
  // Partie creation des table
 "create_table_user" : "CREATE TABLE user (id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255), password VARCHAR(255), token VARCHAR(255))",
 "create_table_dessin" : "CREATE TABLE dessin(id INT AUTO_INCREMENT PRIMARY KEY, url VARCHAR(255), oeuvre VARCHAR(255), page INT)",
 "create_table_category" : "CREATE TABLE category(id INT AUTO_INCREMENT PRIMARY KEY, category VARCHAR(255))",
 "create_table_oeuvre" : "CREATE TABLE author(id INT AUTO_INCREMENT PRIMARY KEY, author VARCHAR(255), titre VARCHAR(255), category INT)",
 //  Partie insertion
 "insert_user" : "INSERT INTO user(name,password,token) VALUES ?",
 "insert_category" : "INSERT INTO category(category) VALUES ?",
 "insert_oeuvre" : "INSERT INTO oeuvre(author,titre,category) VALUES ?",
 "insert_dessin" : "INSERT INTO dessin(url,oeuvre,page) VALUES ?",
 // Partie Select
 "select_all_author": "SELECT name FROM user",
}
module.exports = sql
