var express = require('express')
var router = express.Router() // Ajout du router interne de express

// middleware specifi à ce router
router.use(function timeLog (req, res, next) {
  console.log('Time: ', Date.now())
  next()
})

//Définie la route de la page d'index
router.get('/',(req,res) => res.send("accueil depuis le router externe de l'app"));
router.get('/about',(req,res) => res.send("A propos de ce router"));

module.exports = router
