const express = require('express');
const app = express();
var externe = require('./route_externe.js')



app.get('/', (req,res) => res.send("hello world"));
app.listen(4100,() => console.log("Your app listen on port 4100"));


// Exemple de route classique en GET
app.get('/get',(req,res) => res.send("utilisation de la méthode GET"));

// Exemple de route en POST
app.post('/post'),(req,res) => res.send("utilisation de la méthode POST");

// Exemple de route en ALL avec un Next
app.all('/all', function(req,res, next){
  console.log('Acces en cours...');
  next(); // Envoie à la prochaine requête
});

// Exemple de route plus complexe contenant des paramètres, ici il sont affiché dans le résultats
app.get('/image/:categorie/:id',(req,res) => res.send(req.params));

// Exemple de route avec expressions
app.get('/test*api',(req,res) => res.send("test*api"));
/* Il existe de nombreuse expressions liste non exhaustive ci-dessous:
- ab?cd : Permet des routes du genre : /abc ; /abcd
- ab+cd : Permet des routes du genre : /abcd; /abbcd; /abbbcd; etc..
- ab*cd : Permet des routes du genre : / abTEXTICIcd; /ab123456789cd;
*/

// Exemple de route avec expressions régulière ( Veuillez vous referer aux expressions regulière pour en connaitre tout le potenties)
app.get(/^a/,(req,res) => res.send("le début de l'url commence avec un 'a' "))
/*
Les expressions regulière sont extremement puissante et pemette de traité de très nombreux sujet
que se soit la vérification comme ici dans une url, ou dans un système de vérificaton de mail et mot de passe
*/

// Route avec plusieur CallBack ( rappel leger, tout est ' objet')
app.all('/callback', function(req,res,next){
    console.log("acces en cours")
    next();
  }, function(req,res){
    res.send("callback ")
  }
);

// Route avec plusieur Callback envoyé par une liste de variable
var call0 = function(req,res,next){
  console.log('envoie requête 1') ;
  next();
};
var call1 = function(req,res,next) {
   console.log('envoie requête 2');
   next();
}
var call2 = function(req,res){
  res.send("callback multiple ")
}
app.get('/testCall',[call0,call1,call2]);
/*
on peut bien évidement rajouter d'autre callback qui serait en dehors de la liste
exemple :
  app.get('/testCall',[call0,call1], function(req,res,next){
    console.log('ajout d'un autre callback);
    next()
  }, function(req,res){
    res.send('fin de l'exemple);
  }
  )
*/

// utilisations d'une route externe

app.use('/bird',externe)
