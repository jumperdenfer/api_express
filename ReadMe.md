# Ce projet me permet d'expérimente Express.JS et Node.JS

Vous trouverez ici different petit exercice et test, tous commenter afin de me rendre la lecture plus facile.
Ce projet n'est pas destiné à un débutant souhaitant apprendre express.
Si vous souhaitez utiliser ce projet, ou corriger les eventuels faute d'ortographe votre aide et la bienvenue.

## Objectif du projet :
* Créé un CRUD basique.
* Relié à une base de données MYSQL.
* Renvoyé des objets JSON.

## Liens divers :
* http://expressjs.com/
* https://nodejs.org/en/
* https://regex101.com/ ( Utile pour créé et testé vos regex)
